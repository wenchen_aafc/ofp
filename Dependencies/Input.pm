#/usr/bin/env perl
package Input;
#license
#wet-boew.github.io/wet-boew/License-en.html / wet-boew.github.io/wet-boew/Licence-fr.html

use strict; use warnings;
use Data::Dumper;
use File::Basename;
use Getopt::Long qw(GetOptionsFromArray);

#use lib "/home/AAFC-AAC/hardye/Pre-AODP/Scripts/Dependencies";
use lib dirname(__FILE__)."/../Dependencies";
#use LogFile;
use Config::Simple;

#functions preceeded by '_' are not supposed to be used outside this module
######################################################################################################

sub _deepCopy { #returns a deep copy of a complex reference object
  my $item = shift();
  if (ref $item eq "HASH") {
    my $hashref;
    while(my($key, $val) = each %$item) {
       $hashref->{$key} = _deepCopy($val);
    }
    return $hashref;
  } elsif (ref $item eq "ARRAY") {
    my $arrayref;
    foreach (@$item){
      push(@$arrayref, _deepCopy($_));
    }
    return $arrayref;
  } else {
    return $item;
  }
}

my $_settings         = {}; #the singleton that stores all settings for this run
my $_default_settings = {};
my $_input            = {}; #records command line input through the Getopt::Long module

#Functions///////////////////////////////////////////////////////////////////////////////////
sub Configure { #main control script, returns copy of settings hash
  return if %$_settings;
 
  $_default_settings = _deepCopy(shift());
  my $input_mapping  = shift();
  my $usage_fn       = shift();
  my $format_fn      = shift();  
  $usage_fn = sub {print("No usage message given\n")} unless defined($usage_fn);

  #SO UGLY :'(
  my $config_file = _readCMD($input_mapping, $usage_fn);
  _initFromConfig($config_file) if $config_file;
  _completeWithDefaults($format_fn, $input_mapping, $config_file); 

  return getSettings();
}

sub _readCMD { #process command line args, and see if a config file was specified for use
  my ($input_mapping, $usage_fn) = @_;
  my $input_map = {};
  my @cmd_options;
 
  #print STDERR (Dumper($input_mapping)."\n");
 
  while(my($cmd, $data) = each %$input_mapping) {
    die("Option ".$data->{'config'}." does not have a default\n") 
       unless defined $_default_settings->{$data->{'config'}};
    $input_map->{$cmd}  = sub {$_settings->{$data->{'config'}} = $_input->{$cmd}};
    push(@cmd_options, $cmd.$data->{'type'});
  }

  GetOptionsFromArray(\@main::ARGV, $_input, @cmd_options, 'h|help', 'config=s') or die("$!"); #for a more exhaustive option list, create a config file.

  unless (defined $_input->{'h'} || defined $_input->{'config'}) {
    &{$input_map->{$_}} foreach (keys %$_input);
  }
 
  # commented out to allow for default values
  #my @missing_data = grep {$_ =~ /^INPUT\./ && !defined($_settings->{$_})} keys %$_default_settings;

  #if (defined $_input->{'h'} || (!defined($_input->{'config'}) && @missing_data)) {
  if (defined $_input->{'h'}) {
    &$usage_fn;
    exit;
  }

  return $_input->{'config'};
}

sub _initFromDefault { #copy all settings directly from default
  $_settings = _deepCopy($_default_settings);
}

sub _initFromConfig { #copy settings from a given config file
  my $config_file = shift();  

  unless (-e $config_file) { #bad filename given
    die("[ERROR:] $config_file does not exist\n");
  } else { #read from a config file
    Config::Simple->import_from($config_file, $_settings);
  }
} 


#if any options were not defined in the config file, 
#this will fill in those options with the defaults.
sub _completeWithDefaults {
  my $special_formatting = shift();
  my $input_mapping      = shift();
  my $config_file        = shift();
  %$_settings = (%$_default_settings, %$_settings);

  foreach (grep {$_->{'type'} =~ /@/} values $input_mapping) {
    my $option = $_->{'config'};
    $_settings->{$option} = [$_settings->{$option}] unless (ref $_settings->{$option}); 
  }

  &$special_formatting($_settings, $config_file) if defined($special_formatting);  
}

sub getSettings {
  return _deepCopy($_settings);
}

sub printErrSettings{ #Dump the settings hash to stderr along with a message
  print STDERR (shift()." _Settings::\n".Dumper($_settings)."\n");
}

1;
