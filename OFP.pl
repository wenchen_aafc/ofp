#!/usr/bin/env perl

use strict; use warnings;
use Bio::Seq;
use Bio::SeqIO;
use Bio::Tree::TreeFunctionsI;
use Bio::Tree::Tree;
#use Bio::DB::Taxonomy;
use Bio::TreeIO;
use Bio::Tools::Run::StandAloneBlastPlus;
use File::Basename;
use File::Find;
use File::Spec;
use FindBin;
use Getopt::Long;
use threads;
use threads::shared;
use Thread::Queue;
use Data::Dumper;
use List::MoreUtils qw(uniq);
use Cwd;
use Tie::File;
use IO::String;

use lib "$FindBin::Bin/Dependencies";
use Input;


print("\n".center("", "#")."\n");
print(center("Oligo Fishing Pipeline", " "));
print("\n".center("", "#")."\n\n");
# INITIALIZE VARIABLES ###############################################################################
sub usage {
  print ("\nUSAGE:   perl ".basename($0)." [options]\n");
  print ("OPTIONS:\n");
  print ("         --num_threads <threads>\n");
  print ("          -i <input directory>\n");
  print ("          -o <output directory>\n");
  print ("          -db <blast database name>\n");
  print ("          -p <percent identity cutoff [0-100]>\n");
  print ("          -t <taxonomy directory>\n");
  print ("          -c [--config] <config file> (will override other flags)\n\t\t");
  print("IMPORTANT: Please note that defining run parameters in a configuration file has not been fully validated as of this release\n\n");
  print ("      Note: If no paramaters are given OFP will look for defaults as specified in the manual\n\n");
  exit;
}

# Command line arguments are parsed using Getopt::Long
# See Input.pm for relevant subroutines and documentation

#these defaults are temporary
my $defaults = {
                'EXTERNALS.threads'           => 4,
                'INPUT.oligoTabFile'          => '', # workaround for Input.pm module
                'INPUT.treeFile'              => '', # setting to a non-empty string will override -i directory flag
        		'INPUT.aodpFastaFile'         => '', #
                'INPUT.directory'             => "$FindBin::Bin/Input",
                'OUTPUT.directory'            => "$FindBin::Bin/Output",
                'SEARCH.blastDB'              => "/isilon/biodiversity/users/matwichukm/blastdb/raw454_plates12-15_blastdb_update",
                'SEARCH.identityThreshold'    => 95,
		'EXTERNALS.threshold'         => 0.97,
		'TAXONOMY.directory'          => "/isilon/biodiversity/reference/ncbi_taxonomy/"
               };
my $input_map = {
                 'num_threads'     => {'type' => '=i',  'config' => 'EXTERNALS.threads'          },
                 'i'               => {'type' => '=s',  'config' => 'INPUT.directory'            },
                 'o'               => {'type' => '=s',  'config' => 'OUTPUT.directory'           },
                 'db'              => {'type' => '=s',  'config' => 'SEARCH.blastDB'             },
                 'p'               => {'type' => '=i',  'config' => 'SEARCH.identityThreshold'   },
	            't'               => {'type' => '=s',  'config' => 'TAXONOMY.directory'         }
               };

sub resolvePath {
  my $path      = shift();
  my $base_path = shift();
  our $home = $ENV{'HOME'}."/";

  $path =~ s/^~\//$home/;
  $path = $base_path."/".$path if $path =~ /^\.\./;
  $path = Cwd::abs_path($path) unless $path =~ /^\//;
  $path =~ s/\/$//;
  return $path;
}

sub formatting {
  my $cfg         = shift();
  my $config_file = shift();

  $cfg->{'EXTERNALS.threads'} = $ENV{'NSLOTS'} if defined($ENV{'NSLOTS'});

  #return unless defined($config_file);
  my $config_path;
  if (defined($config_file)) {
    $config_path = dirname($config_file);
  } else {
    $config_path = getcwd; # workaround for when no config file is specified, should not be a big problem
  }

  $cfg->{'INPUT.oligoTabFile'}  = resolvePath($cfg->{'INPUT.oligoTabFile'},  $config_path) if $cfg->{'INPUT.oligoTabFile'}; 
  $cfg->{'INPUT.treeFile'}      = resolvePath($cfg->{'INPUT.treeFile'},      $config_path) if $cfg->{'INPUT.treeFile'};
  $cfg->{'INPUT.aodpFastaFile'} = resolvePath($cfg->{'INPUT.aodpFastaFile'}, $config_path) if $cfg->{'INPUT.aodpFastaFile'};
  $cfg->{'INPUT.directory'}     = resolvePath($cfg->{'INPUT.directory'},     $config_path);
  $cfg->{'OUTPUT.directory'}    = resolvePath($cfg->{'OUTPUT.directory'},    $config_path);
  $cfg->{'TAXONOMY.directory'}  = resolvePath($cfg->{'TAXONOMY.directory'},  $config_path);
}

# get user input from command line (and config file if specifiied)
my $settings = Input::Configure($defaults, $input_map, \&usage, \&formatting);

# Find input files, check that they exist as specified by user
getFile(\$settings->{'INPUT.oligoTabFile'},  $settings->{'INPUT.directory'}, ".tab", "Oligo tab file");
getFile(\$settings->{'INPUT.aodpFastaFile'}, $settings->{'INPUT.directory'}, ".fasta", "Fasta file");
getFile(\$settings->{'INPUT.treeFile'},      $settings->{'INPUT.directory'}, '.tre$|.nwk', "Tree file"); #Match tree or nwk at end of file name
  
die("[ERROR:] Taxonomy dump directory does not exist\n") unless -e $settings->{'TAXONOMY.directory'};
die("[ERROR:] Incomplete taxonomy dump. Need nodes.dmp and names.dmp files\n")
   unless -e $settings->{'TAXONOMY.directory'}."/nodes.dmp" 
       && -e $settings->{'TAXONOMY.directory'}."/names.dmp";
my $nodes_dump_file = $settings->{'TAXONOMY.directory'}."/nodes.dmp";
my $names_dump_file = $settings->{'TAXONOMY.directory'}."/names.dmp";

# INITIALIZE THINGS ##################################################################################
my $start_time    = time();
my $date          = localtime();
my @time_elems    = (split(" ", $date))[1..4];        #(Month,day,time,year)
   $time_elems[2] = join(".", split(":", $time_elems[2])); #hour:min:sec -> hour.min.sec 
   $date          = $time_elems[0].$time_elems[1]."_".$time_elems[2]."_".$time_elems[3]; 
                    #monthday_hour.min.sec_year

#Blast output file name
my $randNum = int(rand(10000));
my $blast_out_file=$randNum.'-'.$date.".tmp.blastn.tab";
my $db_size=0;

my $out_dir = $settings->{'OUTPUT.directory'};
mkdir($out_dir) if ! -e $out_dir;

my $oli_filename = "$out_dir/oligo_matches_$date.tab";
my $lca_filename = "$out_dir/oligo_lca_$date.fasta";
my $otu_filename = "$out_dir/oligo_LCAs_$date.otu";
my $scrap_filename = "$out_dir/oligo_remove_$date.scrap";

my $tree;
eval {
  $tree = Bio::TreeIO->new( -file   => $settings->{'INPUT.treeFile'},
                            -format => 'newick')->next_tree();
};
die("[ERROR:] Problem reading tree file ".$settings->{'INPUT.treeFile'}."\n\n Error message returned by Bio::TreeIO:\n".$@) if $@;

die("Tree not read from file ".$settings->{'INPUT.treeFile'}."\n") unless defined($tree);


# get sequences by blasting target species against database
# verify the existance of the blast database before continuing
my $database = $settings->{'SEARCH.blastDB'};
if ($database =~ /\./){
    if (!(-e "$database.nin" && -e "$database.nhr" && -e "$database.nsq" && -e "$database.nsi" && -e "$database.nsd" && -e "$database.nog")) {
        die("[ERROR:]  Blast database $database is not defined \n");
    }else{
        my $d = `blastdbcmd -db $database -info`;
        $d = (split('\n', $d))[1];
        $d = (split(' ', $d))[0];
        $d =~ s/,//g;
        $db_size=$d;
    }
}else{
    my $fac = Bio::Tools::Run::StandAloneBlastPlus->new();
    if(!($fac->check_db($database))){
        die("[ERROR:] Blast Database $database is not defined \n");
    }else{    
        my $db_prop = $fac->db_info($database);
        $db_size = ($db_prop->{db_num_sequences}-1);
    }
}

print(center(" Getting Sequence Data ", "#") . "\n");


############## MAIN SCRIPT #####################

# get oligos from tab file
# also gets min oligo length
print("Reading oligos from " . $settings->{'INPUT.oligoTabFile'} . "\n");
my $min_oligo_length;
my $oligo_data = readOligos($settings->{'INPUT.oligoTabFile'});
 
# partition oligos into groups for faster search
my $oligo_groups = groupOligos($oligo_data);

my $uniq_seqs = getSequences($settings->{'SEARCH.blastDB'}, $settings->{'INPUT.aodpFastaFile'},
                             $settings->{'SEARCH.identityThreshold'}, $settings->{'EXTERNALS.threads'},
                             $min_oligo_length);
my $num_seqs = scalar(@$uniq_seqs);
print(center(" Found " . $num_seqs . " sequence" . ($num_seqs > 1 ? "s " : " "), "_") . "\n\n");

print(center(" Finding Oligo Matches ","#")."\n"); 

# thread-safe queues
my $seq_queue = Thread::Queue->new(@$uniq_seqs);
my $print_queue = Thread::Queue->new();

# search for oligo matches using multiple threads
my @threads;
foreach (1 .. $settings->{'EXTERNALS.threads'}) {
  push(@threads, threads->create(\&matchOligos)); 
}
$_->join() foreach (@threads);


# print oligos and lca's to file
print(center(" Printing Sequence Data ","#")."\n");
open(my $oli_file,      '>', $oli_filename) 
   or die("$out_dir/oligo_matches.tab could not be opened.\n");
open(my $lca_file,      '>', $lca_filename)  
   or die("$out_dir/oligo_lca.fasta could not be opened.\n");
open(my $scrap_file, '>', $scrap_filename) or die ("$out_dir/oligo_remove.scrap could not be opened.\n");
my (%OTU_counts, %OTUs, %samples);

#print output files
while (my $seq = $print_queue->dequeue_nb()) {
    if($seq->{'scrap'} ne ""){
        print $scrap_file (">".$seq->{'sample'}.'_'.$seq->{'id'}. "\t OLIGO_LCA:".$seq->{'lca'}."\tOLIGOS:"   .$seq->{'oligos'}."\t".$seq->{'scrap'}."\n".$seq->{'seq'}."\n");
    }else{
        print $oli_file      (    $seq->{'sample'}.'_'.$seq->{'id'}. "\tOLIGOS:"   .$seq->{'oligos'}."\n");
        print $lca_file      (">". $seq->{'sample'}.'_'.$seq->{'id'}. "\tOLIGO_LCA:".$seq->{'lca'}   ."\n".$seq->{'seq'}."\n") if defined($seq->{'lca'});
        #Push valid sequences into OTUS and samples for OTU building
        $OTUs{$seq->{'lca'}}                       ||= 1;
        $samples{$seq->{'sample'}}                 ||= 1;
        $OTU_counts{$seq->{'lca'}}{$seq->{'sample'}}++ ;

    }
}
close($oli_file) or die $!;
close($lca_file) or die $!;
close($scrap_file) or die $!;
print(center(" Sequence Data Recorded ", "_")."\n\n");


# Build OTU Lineages and write to OTU table file
print(center(" Building OTU Lineages ", "#")."\n");
my $OTU_queue       = Thread::Queue->new(keys %OTUs);
my $OTU_print_queue = Thread::Queue->new();

@threads = ();
foreach (1 .. $settings->{'EXTERNALS.threads'}) {
  push(@threads, threads->create(\&BuildOTULines, keys %samples)); 
}
$_->join() foreach (@threads);

print(center(" Writing OTU Lineages ","#")."\n");
open(my $otu_file, '>', $otu_filename) 
   or die("$out_dir/oligo_LCAS.otu could not be opened.\n");
print $otu_file ("OTU\t".join("\t", keys %samples)."\tTaxonomy\tOligo Taxonomy\n");

while (my $line = $OTU_print_queue->dequeue_nb()) {
  print $otu_file ($line);
} 
close($otu_file) or die $!;
print(center(" OTU Lineages Recorded ","_")."\n");

###############################################################

############# CLEANUP AND EXIT ###############################
unlink($blast_out_file); 
my $end_time = time();

print("\n");
print(center("", "#")."\n");
print(center(" OFP Completed In:".timeToString($end_time-$start_time)." ", "_")."\n");
print(center("", "#")."\n");

##############################################################




################### SUBROUTINES ##############################

#################### NEW BLAST ########################

sub getSequences {

  # getSequences: create a list of specific sequences to be used in the OFP oligo search
  # Arguments: db        - name of Blast database to get sequences from
  #            in_fasta  - fasta file with sequences for target species (corresponding to input tree file)
  #            threshold - percentage to accept/reject sequences 
  #                        (rejects all sequences with percent identity less than this value)
  #            threads   - number of CPU threads to use when running blast
  # Return value: reference to an array of unique sequences to use in oligo search.
  #               Each array element is a reference to a sequence hash

    my $db = shift();
    my $in_fasta = shift();
    my $threshold = shift();
    my $threads = shift();
    my $min_length = shift(); # length of smallest oligo in tab file - will drop all sequences shorter than this (as they cannot be matched to any oligos)

    print("Using database: $db \t\tNumber of sequences: $db_size\n");

    print("Blasting $in_fasta against database\n");
    my $SEQ_INFILE = Bio::SeqIO->new(-file=> $in_fasta);
    
    while(my $seq = $SEQ_INFILE->next_seq){
        my $string;
        open(my $stringfh, ">", \$string) or die "could not write to string!";
        my $temp_SeqOut = Bio::SeqIO->new(-format=>'fasta', -fh => $stringfh);
        $temp_SeqOut->write_seq($seq);
        my $blast_output= `echo "$string" | blastn -db $settings->{'SEARCH.blastDB'} -dust no -perc_identity $threshold -max_target_seqs $db_size -num_threads $threads -outfmt 6 | cat >> $blast_out_file`;
        close($stringfh);
    }
    
  # get specific sequences from output
  open(my $BLAST_OUT, "<$blast_out_file") or die "Blast output file $blast_out_file could not be opened\n";

  # TODO: parse blast output using bioperl
  # how to call blast with custom output formatting in bioperl?
  print("Processing BLAST output\n");
  my @good_seqs = ();
  while(<$BLAST_OUT>) {
    # get subject sequence name from blast output
    my $seq_name = (split(/\t/, $_))[1];
    push(@good_seqs, $seq_name);
  }
  close($BLAST_OUT) or die $!;

  # remove duplicate sequences in blast output
  # remove sequences that are too short to contain oligos
  my @unique_seqs = uniq(@good_seqs);

  # get full-length sequences from database
  # create a hash for each sequence
  print("Retrieving full length sequences from database\n");
  my @seq_objects = ();
  foreach my $name (@unique_seqs) {
    # get sequence from database
    # can't seem to find a way to do this using BioPerl
    chomp $name;
    my $db_output = `blastdbcmd -db $settings->{'SEARCH.blastDB'} -entry $name -outfmt "%l %s"`; #output is sequence length and sequence data, separated by space
    my $db_pattern = '^([0-9]+)\s+([A-Z\-]+)$';
    unless ($db_output =~ m/$db_pattern/s) {
      # should never run
      warn "Problem reading sequence $name from database $settings->{'SEARCH.blastDB'}\n";
      next;
    }
    
    my $seq_length = $1;
    my $seq_data = $2;
    $seq_data =~ s/\s//g; # should no longer be needed, kept just in case

    # skip sequence if too short
    next unless $seq_length >= $min_length;
   
    # get sample (plate, mid) information from sequence name
    # sequence name format should be id_sample
    my $name_pattern = '^([^_\s]+)_(\S+)';
    my $sample;
    my $id;
    if($name =~ m/$name_pattern/){
        $name =~ m/$name_pattern/;
        ($sample, $id) = ($1, $2);
    }else{
        $sample = $name;
        $id = $name;
    }
    my %seq = ( id => $id,
                len => $seq_length,
                oligos => "",
                seq => $seq_data,
                sample => $sample );
    
    push(@seq_objects, \%seq);
  }
  return \@seq_objects;
}

########################################################



###################### NEW THREADED OLIGO SEARCH ########################

# Outline:
# Call readOligos to read oligo data from input tab file
# Call groupOligos once to partition oligos for faster searching
# matchOligos should be called once for each thread. Each instance will pull sequences off the queue until empty
# Call groupSearch (within matchOligos) once for each sequence

sub matchOligos {
  # matchOligos: threaded subroutine to fish out all oligos matching a given sequence
  # Arguments: none, but the following globals must be defined:
  #              - seq_queue: queue of references to sequence hashes, each thread will pull sequences off the queue until it is exhausted
  #              - oligo_groups: reference to an array of oligo groups, returned by a call to groupOligos
  #              - print_queue: output queue. Sequences that have oligo matches will be pushed onto this queue
  # Return value: none
  
  # process sequences until queue is empty
  while (my $seq = $seq_queue->dequeue_nb()) {
      $seq->{'scrap'}="";
    my $matches = 0;
    my @nodes;
      my @matchingoligos = groupSearch($seq, $oligo_groups);
      my $tempMatchOligoGroups = groupOligos(\@matchingoligos); #Group matching oligos
    # find matching oligos
    foreach my $oligo (@matchingoligos) {
      my $oli_id = $oligo->{'source'} .'-len'. $oligo->{'len'} .'-(s'. $oligo->{'start'} .'e'. $oligo->{'end'} .')';
      $seq->{'oligos'} .= "$oli_id  ";
      push(@nodes, $tree->find_node( -id => $oligo->{'source'}));
        $matches++;
    }

    # if matches are found, get lowest common ancestor and push to print_queue
    if ($matches) {
      # get lca from tree
      ##VERIFY LCA here
        $seq->{'lca'} = ( $matches > 1 ? $tree->get_lca(\@nodes)->id() : $nodes[0]->id() );
        print($seq->{'id'} . " :: $matches "."match".($matches > 1 ? "es" : "")."\t".scalar(@$tempMatchOligoGroups)." Oligo group".(scalar(@$tempMatchOligoGroups) > 1 ? "s" : "")."\n");
        if($matches == 1){ #if oligo is a singleton mark for scrap with reason singleton
            $seq->{'scrap'} = "singleton";
        }elsif(scalar(@$tempMatchOligoGroups) == 1){ #if matching oligos are from only one "group" mark for scrap with reason single oligo group
            if(!(&compareOligoRef($seq))){        #check Blast hits here - If there is no match in blast then mark as scrap
                $seq->{'scrap'} = "Single Oligo Group";
            }
        }
        $print_queue->enqueue($seq);
    }
  }
}

#Testing if the oligos belonging to a sequence match one of the sequences used to fish it from the database originally
sub compareOligoRef{
    #Parameter is the seq
    my $singleCutOff = 98.00;  #cut off for matches from only a single oligo group
    my $seq = shift();
    my $oligoMatchFlag=0; #Assume false - there is no match with blast
    open(blast_file_handle, "<", $blast_out_file);
    while( my $line = <blast_file_handle>){
        if($line =~ /.$seq->{'id'}./){  #Get lines from the blast file that match the sequence ID
            my $ref_seq = (split("\t", $line))[0]; #get query sequence
            chomp($ref_seq);
            if($seq->{'oligos'} =~ /.$ref_seq./){ #do oligos contain the reference sequence
                my $similarity = (split("\t", $line))[2];
                chomp($similarity);
                if($similarity >= $singleCutOff){
                    $oligoMatchFlag=1;  #True there is a valid sequence
                }
            }
        }
    }
    close(blast_file_handle);
    return $oligoMatchFlag;
}

sub groupOligos {
  
  # separate oligos into groups based on common substrings
  # argument: reference to array of oligos, each oligo is a hash with fields for source, start, end, and sequence
  # return value: reference to array of groups, each group is a hash with source, common substring, overlap start/end,
  #   and an array of references to oligos
  
  my $MIN_OVERLAP = 8; # minimum length of common substring for oligo groups
                       # value of 8 gave best runtime results for input oligos of length 25 to 28
  
  my $oligo_data = shift();
  my @groups;

  foreach my $oligo (@$oligo_data) {    
    my $found = 0;

    foreach my $group (@groups) {

      # check source
      next if $group->{'source'} ne $oligo->{'source'};

      # find max/min values for overlap
      my $start = $group->{'start'} >= $oligo->{'start'} ? $group->{'start'} : $oligo->{'start'};
      my $end = $group->{'end'} <= $oligo->{'end'} ? $group->{'end'} : $oligo->{'end'};
      my $new_overlap = $end - $start + 1;

      if ($new_overlap >= $MIN_OVERLAP) {
        # add oligo to group
        push(@{$group->{'oligos'}}, $oligo);

        my $offset = $start - $group->{'start'};
        $group->{'overlap'} = substr $group->{'overlap'}, $offset, $new_overlap;

        $group->{'start'} = $start;
        $group->{'end'} = $end;
        $found = 1;
        last;
      }
    }
    if (!$found) {
      # create new group
      my %newgroup = ( 'source'  => $oligo->{'source'},
                       'overlap' => $oligo->{'seq'},
                       'start'   => $oligo->{'start'},
                       'end'     => $oligo->{'end'},
                       'oligos'  => [$oligo] );
      push(@groups, \%newgroup);
    }
  }
  return \@groups;
}


sub groupSearch {

  # search sequences for oligos using groups constructed by groupOligos
  # improves efficiency by first checking common substrings
  # arguments: reference to a sequence (a hash created in the getSeqData sub),
  #   and a reference to the array of oligo groups (returned by groupOligos)
  # return value: list of references to matching oligos
  
  my $seq = shift();
  my $groups = shift();

  my @matches;

  # check each group of oligos
  foreach my $group (@$groups) {
    my $sub_index = index($seq->{'seq'}, $group->{'overlap'}); # NOTE: efficiency of this line could be improved

    # skip entire group if common substring isn't found
    while ($sub_index != -1) {
      foreach my $oligo (@{$group->{'oligos'}}) {
        # check each oligo in group
        
        my $range_start = $sub_index - $group->{'start'} + $oligo->{'start'};
        my $range_len = $oligo->{'end'} - $oligo->{'start'} + 1;

        if (substr($seq->{'seq'}, $range_start, $range_len) eq $oligo->{'seq'}) {
          # found match
          push(@matches, $oligo);
        }
      }
      # check for other occurences of the substring (not guaranteed to be unique)
      my $overlap_len = $group->{'end'} - $group->{'start'} + 1;
      $sub_index = index($seq->{'seq'}, $group->{'overlap'}, $sub_index + $overlap_len);
    }
  }
  return @matches;
}

sub readOligos {

  # parse data from oligo tab files
  # argument: list of paths to tab files to be read. Lines in the tab files must have the form:
  #           OLIGO_SOURCE-OLIGO_LEN-(sOLIGO_STARTeOLIGO_END) OLIGO_SEQUENCE
  #     e.g.: AF_229469_Alternaria_macrospora_Fungi-len25-(s351e375)  CTTTTGCTTGGTGTTGGGCGTCTTC
  #
  # return value: array of references to oligos

  my @oligo_arr;
  my $PATTERN = '^(.+)-len([0-9]+)-\(s([0-9]+)e([0-9]+)\)\s+([ACGT]+)\s*$'; # edit if necessary
                                                                            # should match the line format of the .max4homo.tab AODP output file
  my $current_min;

  while (my $tab_file = shift()) {
    # process all files in arg list
    open(my $tab_fh, $tab_file) or die("[ERROR:] Could not open oligo tab file $tab_file\n");
    while (my $line = <$tab_fh>) {
      chomp($line);
      die("[ERROR:] Oligo tab file $tab_file could not be read\n") unless $line =~ m/$PATTERN/;
  
      my ($name, $len, $start, $end, $seq) = ($1, $2, $3, $4, $5);
      my %new_oligo = ( source => $name,
                        len => $len,
                        start => $start,
                        end => $end,
                        seq => $seq );
      push(@oligo_arr, \%new_oligo);

      # update minimum length
      $current_min = $len if (!defined($current_min) || $len < $current_min);
    }
    close($tab_fh);
  }
  $min_oligo_length = $current_min;
  return \@oligo_arr;
}

#########################################################################

# OTU TABLE AND LINEAGES ################################################################################

sub BuildOTULines {
  my @samples = @_;
  while (my $OTU = $OTU_queue->dequeue_nb()) {
    print("Gathering OTU data for LCA $OTU\n");
    my $line = "$OTU\t"; 
    foreach my $sample (@samples) {
      $OTU_counts{$OTU}{$sample} ||= 0;
      $line .= $OTU_counts{$OTU}{$sample}."\t";
    }
    $line .= getLineage($OTU)."\t";
    print("Got taxonomic lineage for $OTU\n");
    $line .= getOligoLineage($OTU)."\n";
    print("Got oligo lineage for $OTU\n");
    $OTU_print_queue->enqueue($line);
  }
}

sub getOligoLineage {
  my $otu = shift();
  return join(';', map( {$_->id()} $tree->get_lineage_nodes($otu)), $otu);
}

sub getLineage { #lots of potential to optimize
  my $otu = shift();
  
  my @otu_lineage;
  if ($otu =~ /^Node/) {
    my @species = leaves($tree->find_node(-id => $otu));
    #print("Children of $otu:\n");
    my @species_lineages = map {[lineage($_->id())]} @species; 
    return "Unidentified" if (grep {!ref $_->[0] && $_->[0] =~ /Unknown/} @species_lineages);
    
    my $i = -1;
    my $found_lca = 0;
    while (!$found_lca && defined($species_lineages[0][$i + 1])) {
      $i++;
      foreach my $lin (@species_lineages[1 .. $#species_lineages]) {
        if (!defined($lin->[$i]) || $lin->[$i]->{"name"} ne $species_lineages[0][$i]->{"name"}) {
          $i--; $found_lca = 1; last;
        }
      }
      #print STDERs defined($config_file);
      #  my $config_path   = dirname($config_file); ("$i\n");
    }
    @otu_lineage = @{$species_lineages[0]}[0 .. $i]; 

    unless (@otu_lineage) {
      warn("[WARNING:] Empty lineage for $otu\n");
      warn(join(";", map {$_->{"name"}} @$_)."\n") foreach @species_lineages;
    }

   #update
    if (-$i < scalar @{$species_lineages[0]}) {
      my $complex  = " (complex of: ";
      if (@species < 6) { #magic number
         $complex .= join(';', map {$_->id()} @species);
      } else {
         $complex .= scalar @species ." species";
      }
         $complex .= ")";
      push(@otu_lineage, {"rank" => "complex", "name" => $complex});
    }
    
    ####
  } else {
    @otu_lineage = lineage($otu); 
    return "Unidentified" if $otu_lineage[0] =~ /Unknown/;
  }

  my %rank_to_name;
  $rank_to_name{$_->{"rank"}} = $_->{"name"} foreach @otu_lineage;   
  @otu_lineage = map { $_."__".(defined($rank_to_name{$_}) ? $rank_to_name{$_} : "" ) }         
                     ("k","p","c","o","f","g","s","sp");

  my $j = $#otu_lineage;
     $j-- while ($j >= 0 && $otu_lineage[$j] =~ /^([kpcofgs]|sp)__$/);
  @otu_lineage = @otu_lineage[0 .. $j];

  return "Unidentified" unless @otu_lineage;

  my $taxonomy  = join('; ', @otu_lineage);
     $taxonomy .= $rank_to_name{"complex"} if defined($rank_to_name{"complex"});
  return $taxonomy;
}



sub lineage {
  my @words = split(/[_]/, shift());
  my $species="";
  if(scalar(@words) >= 4){          #if formatted for AODP then extract taxonomy fields from oligo name
    if ($words[3] =~ "|") {
       $words[3] = (split(/\|/, $words[3]))[0];
    }
    if ($words[3] eq "sp") {
       $words[3] = "sp." ;
    }
    $species = join(" ", @words[2,3]);
  }else{
    $species = join(" ", @words[0..$#words]);
  }
  my @taxonomy;  
  my $tax_id = GetIdFromSpecies($species);
  unless (defined $tax_id) {
    warn("[WARNING:] Species $species not found in ".$names_dump_file
        .". Extrapolating from Genus. Perhaps the species name is recorded incorrectly?\n");
    push(@taxonomy, {"rank" => 's', "name" => $species});
    $tax_id = GetIdFromSpecies((split(/ /, $species))[0]);
  }
  unless (defined $tax_id) {
    warn("[WARNING:] Genus ".(split(/ /, $species))[0]." not found in "
        .$names_dump_file.". Lineage Unknown.\n");
    return ("Unknown");
  } 
  while ($tax_id > 1) {
    my($parent_id, my $rank) = GetParentId($tax_id);
    #$tax_id = BinaryGetParent($tax_id);
    my $name = GetNameFromId($tax_id);
      #my $name = BinaryGetName($tax_id);
      return ("Unknown") unless defined($name);
      push(@taxonomy, {"rank" => $rank, "name" => $name});
    #}
    $tax_id = $parent_id;
  }
  return reverse @taxonomy;
}

sub SplitLine {
  return (split(/\t/, shift()))[0,2]; 
}

sub GetIdFromSpecies {
  my $species = shift();
  open(my $gtn, '<', $names_dump_file)
     or die("Could not open ".$names_dump_file."\n");

  while (my $line = <$gtn>) {
    my ($id, $name) = SplitLine($line);   
    if ($name eq "$species") {
      close($gtn); 
      return $id; 
    }  
  }
  close($gtn); 
  return;
}

sub GetNameFromId {
  my $id        = shift();
  open(my $name_dump, '<', $names_dump_file)
     or die("Could not open ".$names_dump_file."\n");

  while (my $line = <$name_dump>) {
    my ($ID, $name) = SplitLine($line);   
    if ($ID == $id && $line =~ /scientific name/) {  ##ADDED scientific name filter
      close($name_dump);
      return $name;
    }
  }
  close($name_dump); 
  return;
}

sub BinaryGetName { #what do if id not found? will id ever not be found? probs not
  my $id        = shift();
  tie(my @name_dump, 'Tie::File', $names_dump_file)
     or die("Could not open ".$names_dump_file."\n");

  my ($lo, $hi) = (0, $#name_dump);
  while (1) {
    my $mid = int(($lo + $hi)/2);
    print STDERR ("Name// $mid\n");
    my $line = $name_dump[$mid];
    #print STDERR ($line."\n");
    my ($ID, undef) = SplitLine($line); 
   
    if ($ID == $id) {
      my ($top, $bottom) = ($mid, $mid);
      $top++    while (SplitLine($name_dump[$top    + 1]))[0] == $id;
      print STDERR ("Top:: $top\n");
      $bottom-- while (SplitLine($name_dump[$bottom - 1]))[0] == $id;
      print STDERR ("Bottom:: $bottom\n");
      for my $i ($bottom .. $top) {
        my (undef, $name) = SplitLine($name_dump[$i]);
        if (split(" ", $name) <= 2) {
          untie(@name_dump); 
          return $name;
        }        
      }
    } 

    if ($id <= $ID) { $hi = $mid;     } 
    else            { $lo = $mid + 1; }
  }
}

sub GetParentId {
  our %rank_map = ('kingdom'    => 'k',
                   'phylum'     => 'p',
                   'class'      => 'c',
                   'order'      => 'o',
                   'family'     => 'f',
                   'genus'      => 'g',
                   'species'    => 's',
                   'subspecies' => 'sp');

  my $id        = shift();
  open(my $node_dump, '<', $nodes_dump_file)
     or die("Could not open ".$nodes_dump_file."\n");

  while (my $line = <$node_dump>) {
    #warn("$line\n");
    my ($ID, $parent_id, $rank) = (split(/\t/, $line))[0,2,4];
    if ($ID == $id) {
      close($node_dump); 
      if (defined $rank_map{$rank}) {
        return ($parent_id, $rank_map{$rank});
      } else {
        return ($parent_id, 'n');
      }
    }
  }
  close($node_dump); 
  return;
}

sub BinaryGetParent {
  my $id        = shift();
  tie(my @node_dump, 'Tie::File', $nodes_dump_file)
     or die("Could not open ".$nodes_dump_file."\n");

  my ($lo, $hi) = (0, $#node_dump);
  while (1) {
    my $mid = int(($lo + $hi)/2);
    print STDERR ("Parent// $mid\n");
    my $line = $node_dump[$mid];
    print STDERR ($line."\n");
    
    if ($hi <= $lo) {
      untie(@node_dump);
      return (split(/\t/, $line))[2];
    }
    if ($id > $mid + 1) { $lo = $mid + 1; }
    else                { $hi = $mid;     }  
  }
}
############################################################################################


# HELPER FUNCTIONS ###################################################################################


sub center {
  # centre text in output, surround with given filler character
  my $_std_width = 102;
  my $message  = shift();
  my $charfill = shift();
  my $offsetr  = $charfill x int(( $_std_width - length($message) )/2);
  my $offsetl  = $charfill x ($_std_width - length($offsetr.$message));

  return $offsetl.$message.$offsetr;
}

sub timeToString {
  use integer;
  my $time  = shift();
  my $sec   = $time % 60;
     $time /= 60; #sec -> min
  my $min   = $time % 60;
     $time /= 60; #min -> hr
  my $hr    = $time % 24;
     $time /= 24; #hr -> day
  my $day   = $time;

  return sprintf("%3sd:%2sh:%2sm:%2ss", $day, $hr, $min, $sec);
}

sub leaves {
  my $node = shift();
  my @leaf_nodes;
  my @children = $node->each_Descendent();
  if (@children) {
    foreach my $kid (@children) {
      push(@leaf_nodes, leaves($kid));
    }
  } else {
    push(@leaf_nodes, $node);
  }
  return @leaf_nodes;
}

sub getFile {

  # getFile: finds the given input file and stores its location in the given settings variable (hash value)
  # arguments: setting      - reference to a scalar variable for the filename to be stored in (should normally be a reference to a hash value)
  #            directory    - directory to search if the filename is not already defined
  #            extension    - extension of the desired file (including the . )
  #            filetype_str - string describing the type of the file (used for error messages only)

  my $setting = shift();
  my $directory = shift();
  my $extension = shift();
  my $filetype_str = shift();

  if ($$setting) {
    # if $$setting is a non-empty string, user has specified the filename manually. Check if it exists and return
    die "[ERROR:] Defined $filetype_str does not exist\n" unless -e $$setting;
    return;
  }

  # search directory for files with the appropriate extension
  opendir(DIR, $directory) or die "[ERROR:] Could not open directory $directory\n";
  my $pattern = $extension.'$';
  my @filenames = grep( !/^\.\.?$/ && /$pattern/ && -f "$directory/$_", readdir(DIR));
    closedir(DIR);
  # must be one and only one suitable file
  die "[ERROR:] Defined $filetype_str cannot be found in $directory\n" if scalar(@filenames) < 1;
  die "[ERROR:] No unique $filetype_str in $directory\n" if scalar(@filenames) > 1;
  $$setting = $directory . "/" . $filenames[0];
}





# HELPER FUNCTIONS ###################################################################################
