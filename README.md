# Oligo-Fishing Pipeline #


Searches directories with oligos.

## Synopsis ##
USAGE:  ./OFP.pl  [options]

COMMAND LINE OPTIONS:
      
--num_threads <threads> (Default: 4)

-i <input directory>
        
-o <output directory>
       
-db <blast database name>
       
-p <percent identity cut off [0-100]> (Default: 95)
       
 -t <taxonomy directory>
        
-c [--config] <path to configuration file>  (A configuration file will override any other flags)
        
-h [-help] display help documentation

Please note that defining run parameters in a configuration file has not been fully validated as of this release

## Command ##
### Minimal: ###
./OFP.pl -db <blast database to search> -t <taxonomy directory>

### Optimal: ###
./OFP.pl --num_threads <threads> -i <input directory> -o <output directory> -db <blast database to search> -t <taxonomy directory>

## Configuration File: ##

```
#!text

--Example:-------------------------------------------------------------------------------
[EXTERNALS]
#Number of threads you wish to use in the analysis.  It is suggested to use 4 threads at a minimum.
threads=1

[INPUT]
#input directory is the directory that contains the input oligo file, tree, and fasta file.
directory=path/to/input/directory

[SEARCH]
#This is the path to and name of the blast database created from the dataset you wish to probe with the oligos.  
#Please consult the manual for correct database formatting. 
blastDB=path/to/search/database
#This is the percent identity used to create the sample specific sub-dataset that is used for oligo fishing.
identityThreshold=95

[TAXONOMY]
#This is the path to the taxonomy files, usually NCBI, used in assigning a taxonomic rank to any fished out sequences.
directory=path/to/taxonomy/Reference
----------------------------------------------------------------------------------------

```
Input methods are interchangeable, but if a configuration file is given its settings will overide any other options set through command line